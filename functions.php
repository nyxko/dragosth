<?php 

function dragos_script_enqueue() {
 	
	wp_enqueue_style('customstyle', get_template_directory_uri() . '/css/dragos.css', array() , '1.0.0', 'all');
	wp_enqueue_script('customjs', get_template_directory_uri() . '/js/dragos.js', array() , '1.0.0', true);

}

add_action( 'wp_enqueue_scripts', 'dragos_script_enqueue');

function dragos_theme_setup() {

	add_theme_support('menus');

	register_nav_menu('primary', 'Primary Header Navigation');
	register_nav_menu('secondary', 'Footer');

}


function theme_dragos_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'theme-dragos' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-dragos' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
	    ) );
}

add_action('init', 'dragos_theme_setup');
add_action( 'widgets_init', 'theme_dragos_widgets_init' );